/*

	1. How do you create arrays in JS?
		*by enclosing a comma-separated list of values inside square brackets []
	
	2. How do you access the first character of an array?
		*we can use the square bracket notation and specify the index of the first element, which is 0.

	3. How do you access the last character of an array?
		*we can use the square bracket notation and specify the index of the last element, which is the length of the array minus 1. 
	
	4. What array method searches for, and returns the index of a given value in an array? This method returns -1 if given value is not found in the array.
		*indexOf()

	5. What array method loops over all elements of an array, performing a user-defined function on each iteration?
		*forEach()

	6. What array method creates a new array with elements obtained from a user-defined function?
		*map()

	7. What array method checks if all its elements satisfy a given condition?
		*every()
	
	8. What array method checks if at least one of its elements satisfies a given condition?
		*some()

	9. True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
		*false

	10. True or False: array.slice() copies elements from original array and returns them as a new array.
		*true

*/	


let students = ["John", "Joe", "Jane", "Jessie"];
console.log(students);

// 1.

function addToEnd(students, newName) {

	if (typeof newName === 'string'){
		students.push(newName);
		return students;
	} else {
		return ("error - can only add strings to an array");
	}
};


// 2.

function addToStart(students, newName) {

	if (typeof newName !== "string") {
	return "error - can only add strings to an array";

	} else {
		students.unshift(newName);
		return students;
	}
};


// 3.

function elementChecker(students, name) {

  	if (students.length === 0) {
    	return "error - passed in array is empty";
  	}

  	for (let n = 0; n < students.length; n++) {
    	if (students[n] === name) {
     	return true;
    	}
  	}
  
  	return false;
};


// 4.

function checkAllStringsEnding(students, char) {

	// Check if array is empty
 	if (students.length === 0) {
    	return "error - array must NOT be empty";
  	}

  	if (students.some((element) => typeof element !== "string")) {
    	return "error - all array elements must be strings";
  	}

  	// Check if 2nd argument is not a string
  	if (typeof char !== "string") {
   	 	return "error - 2nd argument must be of data type string";
  	}

  	// Check if 2nd argument is more than 1 character
  	if (char.length > 1) {
   		return "error - 2nd argument must be a single character";
 	}

 	// Check if every element ends with char
  	return students.every((element) => element.endsWith(char));
};


// 5.

function stringLengthSorter(students) {
	  	
  	// Check if all elements are strings
  	for (let n = 0; n < students.length; n++) {
    	if (typeof students[n] !== 'string') {
      		return 'error - all array elements must be strings';
    	}
  	}
  
  	// Sort the array based on string length
  	students.sort((a, b) => a.length - b.length);

  		return students;
};


// 6.

function startsWithCounter(students, char) {

  	// Check if array is empty
  	if (students.length === 0) {
    	return "error - array must NOT be empty";
  	}

  	// Check if at least one element is NOT a string
  	if (students.some((elem) => typeof elem !== "string")) {
    	return "error - all array elements must be strings";
  	}

  	// Check if 2nd argument is not a string
  	if (typeof char !== "string") {
    	return "error - 2nd argument must be of data type string";
  	}

  	// Check if 2nd argument is more than 1 character
  	if (char.length > 1) {
    	return "error - 2nd argument must be a single character";
  	}

  	// Count elements starting with char argument
  	const count = students.filter((elem) =>
    	elem.toLowerCase().startsWith(char.toLowerCase())
  		).length;

  	return count;
};


// 7.

function likeFinder(students, str) {

  	// Check if array is empty
  	if (students.length === 0) {
    	return "error - array must NOT be empty";
  	}

  	// Check if at least one array element is NOT a string
  	for (let e = 0; e < students.length; e++) {
    	if (typeof students[e] !== "string") {
      		return "error - all array elements must be strings";
    	}
  	}

  	// Check if 2nd argument is not a string
  	if (typeof str !== "string") {
    	return "error - 2nd argument must be of data type string";
  	}

  	// Create a new array to hold matching elements
  	let matchingElements = [];

  	// Loop through array and add matching elements to new array
  	for (let e = 0; e < students.length; e++) {
    
    // Add all elements that contain string(case-insensitive)
    	if (students[e].toLowerCase().includes(str.toLowerCase())) {
      	matchingElements.push(students[e]);
    	}
  	}

  	// Return the new array of matching elements
  	return matchingElements;
};


// 8.

function randomPicker(students) {

  	// Check if the array is empty
  	if (students.length === 0) {
    	return "error - array must NOT be empty";
  	}

  	// Create a random index based on the length of the array
  	const randomIndex = Math.floor(Math.random() * students.length);

  	// Return the element of the created random index
  	return students[randomIndex];
};

